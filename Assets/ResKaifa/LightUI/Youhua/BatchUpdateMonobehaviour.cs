﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatchUpdateBehaviour:MonoBehaviour
{
    string sample_type;
    public string sampleType {
        get {
            if (string.IsNullOrEmpty(sample_type))
                sample_type = "--"+GetType().ToString();
            return sample_type;
        }
    }
    
    public string sample_name;
    public virtual void BatchUpdate() { }
    public virtual void BatchLateUpdate() { }
    public virtual void BatchFixedUpdate() { }

    protected virtual void OnEnable() {

    }
    protected virtual void OnDisable() { }

    protected virtual void Start() { }

    protected virtual void OnDestroy()
    { }

    private void FixedUpdate()
    {
        BatchFixedUpdate();
    }
}
