﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class BatchConfig 
{
    public bool SpawnPoolAutoDestroy;
    public bool AutoProticleProfile;
}
