using System;
using UnityEngine;

public abstract class SpecSingleton<T> where T : class, new()
{
	private static T m_instance;
	public static T Inst
	{
		get
        {
            if (SpecSingleton<T>.m_instance == null)
		    {
                SpecSingleton<T>.m_instance = Activator.CreateInstance<T>();
			    if (SpecSingleton<T>.m_instance != null)
			    {
                    (SpecSingleton<T>.m_instance as SpecSingleton<T>).Init();
			    }
		    }

            return SpecSingleton<T>.m_instance;
        }
	}

	public static void Release()
	{
		if (SpecSingleton<T>.m_instance != null)
		{
            SpecSingleton<T>.m_instance = (T)((object)null);
		}
	}

    public virtual void Init()
    {

    }

    public abstract void Dispose();

}
