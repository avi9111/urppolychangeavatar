﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Profiling;

public class BatchUpdateManager : MonoBehaviour
{
    public BatchConfig Config;
    public static BatchUpdateManager Inst;

    List<BatchUpdateBehaviour> process = new List<BatchUpdateBehaviour>();
    private void Awake()
    {
        Inst = this;
    }
    public void AddUpdate(BatchUpdateBehaviour comp) {
        process.Add(comp);
    }

    private void Update()
    {
        foreach (var pro in process)
        {
            //pro.name 会有 gc,oh no!!!
            //Profiler.BeginSample(pro.name+"-"+pro.sampleType);
            Profiler.BeginSample(pro.sampleType);
            pro.BatchUpdate();
            Profiler.EndSample();
        }
    }
}
