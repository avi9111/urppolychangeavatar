using System;
using System.Collections;
using System.IO;
using UnityEngine;

public class ResMgr : SpecSingleton<ResMgr>
{
    private SpecSingleton<ResMgr> _specSingletonImplementation;

    public IEnumerator LoadResourceAsync<T>(string path, Action<T> callback)where T:UnityEngine.Object {
        string newPath = string.Empty;
        //Resources 目录根目录
        if (Path.GetDirectoryName(path) == "")
            newPath = Path.GetFileNameWithoutExtension(path);
        else
            newPath = Path.GetDirectoryName(path) + "/" + Path.GetFileNameWithoutExtension(path);
        ResourceRequest request = Resources.LoadAsync<GameObject>(newPath);
        yield return request;

        T go = request.asset as T;
        callback(go);
    }
    
    public override void Dispose()
    {
     //   _specSingletonImplementation.Dispose();
    }
    /// <summary>
    /// 暂时只能 Load Sprite
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    public Texture2D LoadResource(string path)
    {
        //TODO:判断路径是否存在（path ??)
        
        //TODO:RES LOAD不能带后缀，把后缀处理掉（集成）
        var sp = Resources.Load(path) as Texture2D;
        if (sp == null)
        {
            Debug.LogError("path=" + path);
            var testObj = Resources.Load(path);
            Debug.LogError(testObj);
        }

        return sp;
    }

    public Mesh LoadMesh(string path)
    {
        var ms = Resources.Load(path) as Mesh;

        Debug.Log(ms);
        if (ms == null)
        {
            Debug.LogError("path=" + path);
            var testObj = Resources.Load(path);
            Debug.LogError(testObj);
        }
        return ms;
    }

}