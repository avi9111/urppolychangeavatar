using System;
using UnityEngine;
using Object = UnityEngine.Object;

public class ReferenceHelper 
{
    /// <summary>
    /// 获取GameObject挂载的对象
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="gameObject"></param>
    /// <param name="key"></param>
    /// <returns>找不到返回null</returns>
    public static bool TryGet<T>(GameObject gameObject, string key,out T result) where T : Object
    {
        result = null;
        if (gameObject.TryGetComponent<ReferenceCollector>(out var rgc))
        {
            result = rgc.Get<T>(key);
            return result != null;
        }
        return false;
    }
    
    /// <summary>
    /// 获取GameObject挂载的对象
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="gameObject"></param>
    /// <param name="key"></param>
    /// <returns>找不到就报错</returns>
    public static T Get<T>(GameObject gameObject,string key)where T : Object
    {
        if(!TryGet<T>(gameObject,key,out var result))
        {
            //throw new Exception($"获取{gameObject.name}的ReferenceCollector key失败, key: {key}");
            Debug.LogError($"获取{gameObject.name}的ReferenceCollector key失败, key: {key}");
        }
        return result;
    }

    public static GameObject Get(GameObject gameObject, string key)
    {
        return Get<GameObject>(gameObject,key);
    }
}
