using UnityEngine;
using UnityEngine.UI;
public class UISnowGuBase : MonoBehaviour
{
    private ReferenceCollector _ref;
    /// <summary>
    /// !!注意，！！！，override Awake()方法时，必须调用base.Awake(),否则会有问题（——ref没初始化）
    /// !!注意，！！！
    /// !!注意，！！！
    /// </summary>
    protected  virtual void Awake()
    {
        _ref = GetComponent<ReferenceCollector>();
    }

    protected  virtual  void  Start()
    {
        
    }


    protected Button GetButton(string key)
    {
        var go = _ref.Get<GameObject>(key);
        return go.GetComponent<Button>();
    }

    protected GameObject GetObj(string key)
    {
        return _ref.Get<GameObject>(key);
    }

    protected Transform GetTrans(string key)
    {
        return _ref.Get<GameObject>(key).transform;
    }

    protected Mesh GetMesh(string key)
    {
        return _ref.Get<Mesh>(key);
    }

    protected RuntimeAnimatorController GetAnimator(string key)
    {
        return _ref.Get<RuntimeAnimatorController>(key);
    }
}
