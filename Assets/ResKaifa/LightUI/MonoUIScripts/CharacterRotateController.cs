using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// 人物展示时的滑动旋转，可旋转模型；据说支持Mobile端操作，待测试；
/// </summary>
public class CharacterRotateController : MonoBehaviour
{
        [Header("点击的区域")]
        [SerializeField] private GameObject target;
        public GameObject touchTargetUI;
        [SerializeField] private float angle = 180f;
        public bool m_IsSwiping = false;
        private Vector3 m_PreviousTouch;

        private RectTransform _rect;

        //貌似 UI是 Overlay模式时不需要传入 camera_ui
        //Camera Cameras_UI;
        // Start is called before the first frame update
        void Start()
        {
            if (target == null)
                target = gameObject;
        
            //    Application.targetFrameRate = 80;
            var touchRawImage = touchTargetUI.GetComponent<RawImage>(); 
            if (touchRawImage != null)
            {
                _rect= touchRawImage.rectTransform;
            }

          //  Cameras_UI = Camera.main;
        }
    
        // Update is called once per frame
        void Update()
        {
    
            if(m_IsSwiping)
            {
                Vector2 diff = Input.mousePosition - m_PreviousTouch;
                
                // Put difference in Screen ratio, but using only width, so the ratio is the same on both
                // axes (otherwise we would have to swipe more vertically...)
                target.transform.Rotate( Vector3.up * -diff.x * angle / (float)Screen.width);
                m_PreviousTouch = Input.mousePosition;
            }

            if (m_IsSwiping == false)
            {
                if (_rect != null)
                {
                    //Debug.Log(" _rect!=null " + Cameras_UI);
                    //貌似 UI是 Overlay模式时不需要传入 camera_ui
                    if(RectTransformUtility.RectangleContainsScreenPoint(_rect, 
                        Input.mousePosition))
                    {    
                        if(Input.GetMouseButton(0)){
                            m_PreviousTouch = Input.mousePosition;
                            m_IsSwiping = true;
                        }
                    }
                }
            }

            // Mouse Input also works on mobile devices.
            // a swipe can still be registered (otherwise, m_IsSwiping will be set to false and the test wouldn't happen for that began-Ended pair)
            if(Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
            {
                m_PreviousTouch = Input.mousePosition;
                m_IsSwiping = true;
            } 
            else if(Input.GetMouseButtonUp(0))
            {
                m_IsSwiping = false;
            }
    
        }
}
