using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;
using UnityEditorInternal;
/// <summary>
/// 返回排序的gameObject, 第一个作为基类
/// </summary>
public class SkinnedSelectFirstSelectWindow:EditorWindow
{
    public Action<List<GameObject>> BeforeClose;
    //private GameObject _selected;
    private ReorderableList _stringArray;
    public List<GameObject> SelGameObjects = new List<GameObject>();
    private void Awake()
    {
        _stringArray = new ReorderableList(SelGameObjects, typeof(GameObject)
            , true, true, false, false);

        _stringArray.drawHeaderCallback += (Rect rect) =>
        {
            
            GUI.Label(rect, "[合并多个SkinnedMesh]通过拖动排序，第一个为基底");
        };
    }

    private void OnDestroy()
    {
        BeforeClose = null;
    }

    private void OnGUI()
    {
        if (_stringArray == null)
        {
            this.Close();//做一个保护”小处理“，当_stringArray == null 时，意味着是re-compile，意外为 null，所以可以直接退出
            return;
        }

        _stringArray.DoLayoutList();
        if (GUILayout.Button("选择"))
        {
            // foreach (var go in SelGameObjects)    
            // {
            //     Debug.LogError(go.name);
            // }
            Debug.LogError("on 选择 Button");
            List<GameObject> bodies = new List<GameObject>();
            bodies.AddRange(SelGameObjects);
            BeforeClose?.Invoke(bodies);
            this.Close();
        }
    }
}
