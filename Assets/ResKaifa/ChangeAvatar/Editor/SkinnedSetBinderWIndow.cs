using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

public class SkinnedSetBinderWIndow : EditorWindow
{
    private ReorderableList _stringArray;
    public List<Transform> bones = new List<Transform>();
    private Vector2 paintScrollPosition;
    /// <summary>
    /// 欲绑定（追加)的对象（prefab)名字
    /// </summary>
    public string bindObjectName
    {
        get
        {
            if (bindObject == null)
                return "NULL";
            else
                return bindObject.name;
        }
    }

    /// <summary>
    /// added object (root)
    /// </summary>
    public GameObject bindObject;
    public Action<GameObject,Transform> BeforeCloseExecution;
    private Transform selTrans;
    private void Awake()
    {
        _stringArray = new ReorderableList(bones, typeof(Transform)
            , true, true, false, false);
        
        //自定义列表名称
        _stringArray.drawHeaderCallback = (Rect rect) =>
        {
            GUI.Label(rect, string.Format("选择 {0} 所绑定骨骼",bindObjectName));
        };
        _stringArray.onSelectCallback += OnSelected;
        
        
    }

    private void OnDestroy()
    {
        BeforeCloseExecution = null;
    }

    void OnSelected(ReorderableList lst)
    {
        #if UNITY_2021_1_OR_NEWER
        selTrans = bones[lst.selectedIndices[0]];
        Debug.LogError("select one");
        #endif
    }

    private void OnGUI()
    {
        GUILayout.Label("1.若模型，骨骼有缩放，会做一个反向缩放");
        GUILayout.Label("2.也处理position，平移。。。。");
        GUILayout.Label("3.暂无处理带旋转的模型。。。。有够恶心的");
        paintScrollPosition = GUILayout.BeginScrollView(paintScrollPosition, GUILayout.ExpandHeight(true));
        _stringArray.DoLayoutList();

        if (GUILayout.Button("选择该节点"))
        {
            BeforeCloseExecution?.Invoke(bindObject, selTrans);
            this.Close();
            
        }
        
        GUILayout.EndScrollView();
        
    }
}
