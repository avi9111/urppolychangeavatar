using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
//using UnityEngine.TextCore.Text;

public static class SkinnedSetMeshBoneTools
{
    public enum CombinType
    {
        Identity,
        NoneScale,//但offset 要继续
        /// <summary>
        /// 现在已修复，版本（但其实没有Yoffset，都修复了)
        /// </summary>
        YOffset,
        Normal,
        GuAili
    }

    private static CombinType _combinType;
    private const string kGameObjectSkinnedGroup = "GameObject/Skinned/";
    // /// <summary>
    // /// 只能绑定第一个骨骼，若需要绑定头或手，需要用 (monobehaviour)CharacterSkinnedAdd.cs
    // /// </summary>
    // //[MenuItem(kGameObjectSkinnedGroup+"Set Mesh BoneWeight (第0个节点)", priority = 0)]
    // static void Init()
    // {
    //     
    //     if (Selection.activeGameObject == null)
    //     {
    //         Debug.LogWarning("请先选择gameObject，再点击/Skinned/Set Mesh BoneWeight");
    //         return;
    //     }
    //     if (Selection.activeGameObject.GetComponent<SkinnedMeshRenderer>() == null)
    //     {
    //         Debug.LogWarning("缺 SkinnedMeshRenderer");
    //         return;
    //     }
    //
    //     //TODO:提示不可还原，是否还要设置
    //     //CharacterSkinned4Add.SetSkinnedBoneWeight((Selection.activeGameObject.GetComponent<SkinnedMeshRenderer>()));
    // }

    [MenuItem(kGameObjectSkinnedGroup+"Log SkinnedMeshRender Bones", priority = 0)]
    static void LogSkinnedBones()
    {
        if (Selection.activeGameObject == null) return;
        SkinnedUtil.ReadAllBones(Selection.activeGameObject);
    }
    [MenuItem(kGameObjectSkinnedGroup + "合并2个SkinnedMesh（修复缩放，也平移+暂无旋转)")]
    static void Combin2SkinnedNoScale()
    {
        _combinType = CombinType.YOffset;
        Combin2Skinned();
    }

    [MenuItem(kGameObjectSkinnedGroup + "合并2个SkinnedMesh（测，不做任何缩放)")]
    static void Combin2SkinnedByOffset()
    {
        _combinType = CombinType.Identity;
        Combin2Skinned();
    }
    [MenuItem(kGameObjectSkinnedGroup + "合并2个SkinnedMesh_Origin（y轴不翻转，想正常缩放，蛋白应该做了2次缩放）")]
    static void Combin2SkinnedByNormal()
    {
        _combinType = CombinType.Normal;
        Combin2Skinned();
    }
    
    [MenuItem(kGameObjectSkinnedGroup + "合并2个Skinned谷爱你，模型")]
    static void Combin2SkinnedByGuAili()
    {

        _combinType = CombinType.GuAili;
        Combin2Skinned();
    }


    static void Combin2Skinned()
    {
        if (Selection.gameObjects!=null && Selection.gameObjects.Length < 2)
        {
            EditorUtility.DisplayDialog("", "必须至少选2对象（带skinnedMeshRender）", "Ok");
            return;
        }

        foreach (var go in Selection.gameObjects) 
        {
            if (go.GetComponentInChildren<SkinnedMeshRenderer>() == null)
            {
                EditorUtility.DisplayDialog("", "所选对象必须都带 skinnedMeshRender", "Ok");
                return;
            }
        }

        var popUp = EditorWindow.GetWindow<SkinnedSelectFirstSelectWindow>();
        Debug.LogWarning("cc " + Selection.gameObjects.Length);
        popUp.SelGameObjects.Clear();
        popUp.SelGameObjects.AddRange(Selection.gameObjects);
        popUp.BeforeClose = null;
        popUp.BeforeClose += OnSelectFirstSelectClose;//執行合并
        popUp.Show();
      
    }


    static void OnSelectFirstSelectClose(List<GameObject> bodies)
    {

        Debug.LogError("OnSelectFirstSelectClose time=" + Time.realtimeSinceStartup );
        if (_combinType == CombinType.Identity)
            UCharacterController.CombinBodies(bodies, -1);
        else if (_combinType == CombinType.Normal)
            UCharacterController.CombinBodies(bodies, 1,false,false);
        else if(_combinType == CombinType.NoneScale)
            UCharacterController.CombinBodies(bodies,2);
        else if (_combinType == CombinType.YOffset)
            UCharacterController.CombinBodies(bodies, 0);
        else if(_combinType == CombinType.GuAili)
        {
            //合并后暂时不 Destroy，看看
            UCharacterController.CombinBodies(bodies, 3,false,false);
        }
    }
    [MenuItem(kGameObjectSkinnedGroup+"测试(Undo)")]
    static void TestUndo()
    {

        if (Selection.activeGameObject == null) return;
        var trans = Selection.activeGameObject.transform;
        Undo.RecordObject(trans,"move test for undo...");
        trans.position += new Vector3(0, 3, 0);
    }

}
