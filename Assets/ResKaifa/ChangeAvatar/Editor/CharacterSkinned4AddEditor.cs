using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Object = System.Object;

[CustomEditor(typeof(CharacterSkinned4Add))]
public class CharacterSkinnedAddEditor : Editor
{
    private CharacterSkinned4Add _target;
    private void Awake()
    {
        _target = target as CharacterSkinned4Add;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        GUILayout.Label("会改变目标mesh！！慎用！！之后再优化");
        GUILayout.Space(5);
        var are = GUILayoutUtility.GetRect(0f, 100f, GUILayout.ExpandWidth(true));
        GUI.contentColor = Color.white;
        GUI.Box(are, "\n拖拽区域");
        EditorGUILayout.Space();
        
        var eventType = Event.current.type;
        switch (eventType)
        {
            case EventType.DragUpdated:
            case EventType.DragPerform:
                if (are.Contains(Event.current.mousePosition))
                {
                    DragAndDrop.visualMode = DragAndDropVisualMode.Copy;
                    if (eventType == EventType.DragPerform)
                    {
                        DragAndDrop.AcceptDrag();
                        for (int i = 0; i < DragAndDrop.objectReferences.Length; ++i)
                        {
                            if (i >= 1)
                            {
                                Debug.LogError("拖拽超过一个，只取第一个");
                                continue;
                                
                            }

                            var o = DragAndDrop.objectReferences[i];
                            if (o is GameObject && _target.gameObject.GetComponentInChildren<SkinnedMeshRenderer>()!=null)
                            {
                                _target.resetMesh = null;
                                ShowSelectBoneWindow(o);
                            }
                            else if (o is Mesh)
                            {
                                
                                Debug.LogError("is Mesh...");
                                _target.resetMesh = (Mesh) o;
                                
                                ShowSelectBoneWindow(_target.gameObject);
                            }
                            else
                            {
                                Debug.LogWarning("拖了一个？？" + o.GetType());
                             //   Debug.LogError(o);
                            }
                        }
                    }
                    Event.current.Use();
                }

                break;
            default:
                break;
        }
        
        // if (GUILayout.Button("（测）绑定原后输出"))
        // {
        //     BindAndSaveMesh();
        // }

        if (GUILayout.Button("Save ShareMesh"))
        {
            //SaveMesh();
            SaveCloneMesh();
        }
    }

    void ShowSelectBoneWindow(Object o)
    {
        var popUp = EditorWindow.GetWindow<SkinnedSetBinderWIndow>();
        popUp.bindObject = (GameObject)o;
        popUp.bones.Clear();
        var bones =_target.gameObject.GetComponentInChildren<SkinnedMeshRenderer>()
            .bones;
        popUp.bones.AddRange(bones);
        popUp.BeforeCloseExecution -= _target.BeforeCloseSkinnedBindderWindow;
        popUp.BeforeCloseExecution += _target.BeforeCloseSkinnedBindderWindow;
        popUp.Show();
    }
    // void BindAndSaveMesh()
    // {
    // }

    void SaveMesh()
    {
        SkinnedMeshRenderer smr = _target.gameObject.GetComponentInChildren<SkinnedMeshRenderer>();
        var name = smr.sharedMesh.name; 
        AssetDatabase.CreateAsset(smr.sharedMesh, "Assets/" + name + ".asset");
        
    }
    
    void SaveCloneMesh(int sub=-1)
    {
        SkinnedMeshRenderer smr = _target.gameObject.GetComponentInChildren<SkinnedMeshRenderer>();
        var name = smr.sharedMesh.name;
        if (name.Contains("_added") == false || name.Contains("_copy"))
        {
            name = name + "_copy";
        }
        var mesh = new Mesh();
        Mesh meshAdded = null;
        if (sub == -1)
        {
            meshAdded = smr.sharedMesh;
        }
        else
        {
            //meshAdded = smr.sharedMesh.GetSubMesh(sub).;
        }

        if (_target.copyMeshScale > 0)
        {
            mesh.vertices = new Vector3[meshAdded.vertices.Length];
            var matrix = CharacterSkinned4Add.GetScaleValMatrix(1/_target.copyMeshScale);
            for (int i = 0; i < meshAdded.vertices.Length;i++)
            {
                mesh.vertices[i] = matrix * meshAdded.vertices[i];
            }
        }else if (_target.copyMeshScale < 0)
        {
            mesh.vertices = new Vector3[meshAdded.vertices.Length];
            var matrix = CharacterSkinned4Add.GetScaleValMatrix(-_target.copyMeshScale);
            for (int i = 0; i < meshAdded.vertices.Length;i++)
            {
                mesh.vertices[i] = matrix * meshAdded.vertices[i];
            }
        }
        else
        {
            mesh.vertices = meshAdded.vertices;    
        }

        mesh.uv = meshAdded.uv;
        mesh.uv2 = meshAdded.uv2;
        mesh.uv3 = meshAdded.uv3;
        mesh.uv4 = meshAdded.uv4;
        mesh.uv5 = meshAdded.uv5;
        mesh.uv6 = meshAdded.uv6;
        mesh.uv7 = meshAdded.uv7;
        mesh.uv8 = meshAdded.uv8;
        mesh.tangents = meshAdded.tangents;
        mesh.normals = meshAdded.normals;
        mesh.colors = meshAdded.colors;
        mesh.colors32 = meshAdded.colors32;
        mesh.bounds = meshAdded.bounds;//这个应该是有一个中心点（bounds center?还是什么不确定)，控制位置的
        mesh.bindposes = meshAdded.bindposes;
        mesh.boneWeights = meshAdded.boneWeights;
        mesh.triangles = meshAdded.triangles;
        mesh.RecalculateNormals();
        mesh.RecalculateTangents();
        mesh.RecalculateBounds();
        AssetDatabase.CreateAsset(mesh, "Assets/" + name + ".asset");
    }

}
