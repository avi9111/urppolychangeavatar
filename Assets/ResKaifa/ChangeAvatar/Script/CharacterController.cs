﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;

public class UCharacterController {

    /// <summary>
    /// GameObject reference
    /// </summary>
	public GameObject Instance = null;
	public GameObject WeaponInstance = null;

    /// <summary>
    /// Equipment informations
    /// </summary>
	public string skeleton;
	public string equipment_head;
	public string equipment_chest;
	public string equipment_hand;
	public string equipment_feet;

	private Dictionary<int, string> bodyStrs = new Dictionary<int, string>();
    /// <summary>
    /// The unique id in the scene
    /// </summary>
	public int index;

    /// <summary>
    /// Other vars
    /// </summary>
	public bool rotate = false;
	public int animationState = 0;

	private Animation animationController = null;

	public UCharacterController (int index,string skeleton, string weapon, string head, string chest, string hand, string feet, bool combine = false) {

		//Creates the skeleton object
		Object res = Resources.Load ("Prefab/" + skeleton);
		this.Instance = GameObject.Instantiate (res) as GameObject;
		this.index = index;
		this.skeleton = skeleton;
		this.equipment_head = head;
		this.equipment_chest = chest;
		this.equipment_hand = hand;
		this.equipment_feet = feet;
		
		string[] equipments = new string[4];
		equipments [0] = head;
		equipments [1] = chest;
		equipments [2] = hand;
		equipments [3] = feet;
		
        // Create and collect other parts SkinnedMeshRednerer
		SkinnedMeshRenderer[] meshes = new SkinnedMeshRenderer[4];
		GameObject[] objects = new GameObject[4];
		for (int i = 0; i < equipments.Length; i++) {
			
			res = Resources.Load ("Prefab/" + equipments [i]);
			objects[i] = GameObject.Instantiate (res) as GameObject;
			meshes[i] = objects[i].GetComponentInChildren<SkinnedMeshRenderer> ();
		}
		
        // Combine meshes
		App.Game.CharacterMgr.CombineSkinnedMgr.CombineObject (Instance, meshes, 0, combine);

        // Delete temporal resources
        for (int i = 0; i < objects.Length; i++) {
			
			GameObject.DestroyImmediate (objects [i].gameObject);
		}
		
		// Create weapon
		res = Resources.Load ("Prefab/" + weapon);
		WeaponInstance = GameObject.Instantiate (res) as GameObject;
		
		Transform[] transforms = Instance.GetComponentsInChildren<Transform>();
		foreach (Transform joint in transforms) {
			if (joint.name == "weapon_hand_r") {// find the joint (need the support of art designer)
				WeaponInstance.transform.parent = joint.gameObject.transform;
				break;
			}	
		}

        // Init weapon relative informations
		WeaponInstance.transform.localScale = Vector3.one;
		WeaponInstance.transform.localPosition = Vector3.zero;
		WeaponInstance.transform.localRotation = Quaternion.identity;

        // Only for display
		animationController = Instance.GetComponent<Animation>();
		PlayStand();
	}

	public void ChangeHeadEquipment (string equipment,bool combine = false)
	{
		ChangeEquipment (0, equipment, combine);
	}
	
	public void ChangeChestEquipment (string equipment,bool combine = false)
	{
		ChangeEquipment (1, equipment, combine);
	}
	
	public void ChangeHandEquipment (string equipment,bool combine = false)
	{
		ChangeEquipment (2, equipment, combine);
	}
	
	public void ChangeFeetEquipment (string equipment,bool combine = false)
	{
		ChangeEquipment (3, equipment, combine);
	}
	
	public void ChangeWeapon (string weapon)
	{
		Object res = Resources.Load ("Prefab/" + weapon);
		GameObject oldWeapon = WeaponInstance;
		WeaponInstance = GameObject.Instantiate (res) as GameObject;
		WeaponInstance.transform.parent = oldWeapon.transform.parent;
		WeaponInstance.transform.localPosition = Vector3.zero;
		WeaponInstance.transform.localScale = Vector3.one;
		WeaponInstance.transform.localRotation = Quaternion.identity;
		
		GameObject.Destroy(oldWeapon);
	}

	public void SafeSetBodyStrs(int index, string value)
	{
		if (bodyStrs.ContainsKey(index))
		{
			bodyStrs[index] = value;
		}
		else
		{
			bodyStrs.Add(index,value);
		}
	}

	public void AddOrSetBody(bool combine = false)
	{
		AddOrSetBody(4, "index==4 ，所以不重要，可为空字串", combine);
	}
	/// <summary>
	/// 合并 Matrial 才需要，暂时无用
	/// </summary>
	/// <param name="index"></param>
	/// <param name="body_path"></param>
	/// <param name="combine"></param>
	public void AddOrSetBody(int index, string body_path, bool combine = false)
	{
		switch (index) {
			
			case 0:
				equipment_head = body_path;
				break;
			case 1:
				equipment_chest = body_path;
				break;
			case 2:
				equipment_hand = body_path;
				break;
			case 3:
				equipment_feet = body_path;
				break;
		}
		SafeSetBodyStrs(0,equipment_head);
		SafeSetBodyStrs(1,equipment_chest);
		SafeSetBodyStrs(2,equipment_hand);
		SafeSetBodyStrs(3,equipment_feet); 
		SafeSetBodyStrs(4,"fffff");
		//
		// string[] equipments = new string[4];
		// equipments [0] = equipment_head;
		// equipments [1] = equipment_chest;
		// equipments [2] = equipment_hand;
		// equipments [3] = equipment_feet;
		
		string[] equipments = bodyStrs.Values.ToArray();
		
		Object res = null;
		SkinnedMeshRenderer[] meshes = new SkinnedMeshRenderer[equipments.Length];
		GameObject[] objects = new GameObject[equipments.Length];
		for (int i = 0; i < equipments.Length; i++) {
			
			res = Resources.Load ("Prefab/" + equipments [i]);
			if (res == null) continue;
			Debug.LogError("add body skinMeshRender =" + i);
			objects[i] = GameObject.Instantiate (res) as GameObject;
			meshes[i] = objects[i].GetComponentInChildren<SkinnedMeshRenderer> ();
		}
		//TODO:现在不用这个方法了吧？？
		App.Game.CharacterMgr.CombineSkinnedMgr.CombineObject (Instance, meshes, 0,combine);
		
		for (int i = 0; i < objects.Length; i++) {
			
			GameObject.DestroyImmediate(objects[i].gameObject);
		}
	}

	/// <summary>
	/// 合并 2 ~ 多个 SkinnedMeshRenderer
	/// </summary>
	/// <param name="objects">第一个 gameObject 作为基底，其他 meshes 合并到这个基底</param>
	/// <param name="type">选择合并前的转换矩阵，0== y offset ; 1 == normal ;-1即是没有缩放</param>
	/// <param name="combine">是否合并材质</param>
	public static void CombinBodies(List<GameObject> objects,int type,bool combine = false,bool destroyAfterCombine=true)
	{
		GameObject sourceGo = null; 
		//SkinnedMeshRenderer[] meshes = new SkinnedMeshRenderer[objects.Count-1];
		SkinnedMeshRenderer[] meshes = new SkinnedMeshRenderer[objects.Count];
		for (int i = 0; i < objects.Count; i++) {

			if (i == 0)
			{
				sourceGo = objects[i];
				//continue;
			}
			
			//第一个不合并
			//meshes[i-1] = objects[i].GetComponentInChildren<SkinnedMeshRenderer> ();
			//第一个必须合并！！注意！！！
			meshes[i] = objects[i].GetComponentInChildren<SkinnedMeshRenderer> ();
		}

		//Undo.IncrementCurrentGroup();
		App.Game.CharacterMgr.CombineSkinnedMgr.CombineObject (sourceGo, meshes, type,combine);
		
		
		if (destroyAfterCombine)
		{
			//Editor 模式下，其实不能删除：
			//InvalidOperationException: Destroying a GameObject inside a Prefab instance is not allowed.
			for (int i = 1; i < objects.Count; i++)
			{
				//Undo.DestroyObjectImmediate(objects[i].gameObject);
				GameObject.DestroyImmediate(objects[i].gameObject);
			}

			//Undo.RecordObject(sourceGo,"Combin SkinnedMesh by other ");
		}
	}

	public void ChangeEquipment (int index, string equipment,bool combine = false)
	{
		switch (index) {
			
		case 0:
			equipment_head = equipment;
			break;
		case 1:
			equipment_chest = equipment;
			break;
		case 2:
			equipment_hand = equipment;
			break;
		case 3:
			equipment_feet = equipment;
			break;
		}
		
		string[] equipments = new string[4];
		equipments [0] = equipment_head;
		equipments [1] = equipment_chest;
		equipments [2] = equipment_hand;
		equipments [3] = equipment_feet;
		
		Object res = null;
		SkinnedMeshRenderer[] meshes = new SkinnedMeshRenderer[4];
		GameObject[] objects = new GameObject[4];
		for (int i = 0; i < equipments.Length; i++) {
			
			res = Resources.Load ("Prefab/" + equipments [i]);
			objects[i] = GameObject.Instantiate (res) as GameObject;
			meshes[i] = objects[i].GetComponentInChildren<SkinnedMeshRenderer> ();
		}
		
		App.Game.CharacterMgr.CombineSkinnedMgr.CombineObject (Instance, meshes,0, combine);
		
		for (int i = 0; i < objects.Length; i++) {
			
			GameObject.DestroyImmediate(objects[i].gameObject);
		}
	}

	public void PlayStand () {

		animationController.wrapMode = WrapMode.Loop;
		animationController.Play("breath");
		animationState = 0;
	}
	
	public void PlayAttack () {
		
		animationController.wrapMode = WrapMode.Once;
		animationController.PlayQueued("attack1");
		animationController.PlayQueued("attack2");
		animationController.PlayQueued("attack3");
		animationController.PlayQueued("attack4");
		animationState = 1;
	}
	
	// Update is called once per frame
	public void Update () {
	
		if (animationState == 1)
		{
			if (! animationController.isPlaying)
			{
				PlayAttack();
			}
		}
		if (rotate)
		{
			Instance.transform.Rotate(new Vector3(0,90 * Time.deltaTime,0));
		}
	}
}
