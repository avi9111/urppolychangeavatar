﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections.LowLevel.Unsafe;

public class UCombineSkinnedMgr {

    /// <summary>
    /// Only for merge materials.
    /// </summary>
	private const int COMBINE_TEXTURE_MAX = 512;
	private const string COMBINE_DIFFUSE_TEXTURE = "_MainTex";
	/// <summary>
	/// Snow 谷的模型缩放
	/// </summary>
	/// <param name="smr"></param>
	/// <returns></returns>
	Matrix4x4 GetScaleGuMatrix(SkinnedMeshRenderer smr)
	{
		Matrix4x4 matrix = Matrix4x4.identity;
		var trans = smr.transform;
		var scale = trans.lossyScale;
		matrix.m00 = scale.x;
		Debug.LogError($"测 {smr.name} 真实x3 Scale={scale.x}");
		matrix.m11 = scale.y;
		matrix.m22 = scale.z;
		return matrix;
	}

	Matrix4x4 GetScalePosOriginMatrix(SkinnedMeshRenderer smr)
	{
		Matrix4x4 matrix = Matrix4x4.identity;
		var trans = smr.transform;
		var scale = trans.lossyScale;
		//不是要global scale 也不是local scale
		//而是这个缩放，要把自身排除
		//这是因为自身skinnedMeshRender 的 shareMesh bindPos 已经使用过一次，
		//var finalScale =  trans.lossyScale / trans.localScale;
		matrix.m00 = scale.x/trans.localScale.x;
		Debug.LogError("测 真实 Scale=" + scale.x/trans.localScale.x);
		matrix.m11 = scale.y/trans.localScale.y;
		matrix.m22 = scale.z/trans.localScale.z;
		
		var position = trans.position;
		matrix.m03 = position.x;
		matrix.m13 = position.y;
		matrix.m23 = position.z;
		return matrix;
	}

	Matrix4x4 MatrixAddPos(Matrix4x4 matrix,Transform trans)
	{
		var position = trans.position;//world position
		matrix.m03 = position.x;
		matrix.m13 = position.y;
		matrix.m23 = position.z;
		return matrix;
	}

	Matrix4x4 MatrixTurnXYZ(Matrix4x4 matrix4X4, Transform trans)
	{
		return matrix4X4;
	}

	/// <summary>
	/// 
	/// </summary>
	/// <param name="transform">取 Root 的缩放即可</param>
	Matrix4x4 GetScalePosMatrix(SkinnedMeshRenderer smr)
	{
		Matrix4x4 matrix = Matrix4x4.identity;
		var trans = smr.transform;
		var scale = trans.lossyScale;
		
		matrix.m00 = scale.x;
		matrix.m11 = scale.y;
		matrix.m22 = scale.z;
		
		var position = trans.position;
		matrix.m03 = position.x;
		matrix.m23 = -position.y;//y 和 z 调转？？ 并且y 需要反方向 ??
		matrix.m13 = position.z;
		return matrix;
	}

	Matrix4x4 GetPosMatrix(SkinnedMeshRenderer smr)
	{
		var matrix = Matrix4x4.identity;
		var trans = smr.transform;
		var position = trans.position;
		matrix.m03 = position.x;
		//matrix.m13 = position.y;
		matrix.m23 = -position.y;//y 和 z 调转？？ 并且y 需要反方向 ??
		matrix.m13 = position.z;
		
		return matrix;
	}

	/// <summary>
    /// Combine SkinnedMeshRenderers together and share one skeleton.
    /// Merge materials will reduce the drawcalls, but it will increase the size of memory. 
    /// </summary>
    /// <param name="skeleton">combine meshes to this skeleton(a gameobject)</param>
    /// <param name="meshes">meshes need to be merged</param>
    /// <param name="type">选择合并前的转换矩阵，0== y offset ; 1 == normal ;-1即是没有缩放</param>
    /// <param name="combine">merge materials or not</param>
	public void CombineObject (GameObject skeleton, SkinnedMeshRenderer[] meshes,int type = 0, bool combine = false){

		// Fetch all bones of the skeleton
		List<Transform> transforms = new List<Transform>();
		transforms.AddRange(skeleton.GetComponentsInChildren<Transform>(true));

		List<Material> materials = new List<Material>();//the list of materials
		List<CombineInstance> combineInstances = new List<CombineInstance>();//the list of meshes
		List<Transform> bones = new List<Transform>();//the list of bones

		// Below informations only are used for merge materilas(bool combine = true)
		List<Vector2[]> oldUV = null;
		Material newMaterial = null;
		Texture2D newDiffuseTex = null;

		// Collect information from meshes
		for (int i = 0; i < meshes.Length; i ++)
		{
			SkinnedMeshRenderer smr = meshes[i];
			if (smr.sharedMesh == null) continue;
//			materials.AddRange(smr.materials); // Collect materials//会有个错误提示，导致一些异常
			materials.AddRange(smr.sharedMaterials); // Collect materials
			if (type == 0)
			{
				Debug.LogError("测 combine （offsetY,type==0）时,scale=" + (smr.transform.lossyScale.x / smr.transform.localScale.x)/skeleton.transform.localScale.x);
			}
			
			// Collect meshes
			#region Mesh 坐标转换
			for (int sub = 0; sub < smr.sharedMesh.subMeshCount; sub++)
			{
				CombineInstance ci = new CombineInstance();
				ci.mesh = smr.sharedMesh;
				if (type == 0)
				{
					//	ci.transform = GetScalePosMatrix(smr);//错误的一个方法(Matrix not right)
					ci.transform = Matrix4x4.identity; //正確的 Matrix 
					ci.transform *= CharacterSkinned4Add.GetScaleValMatrix(smr.transform.lossyScale.x /
					                                                       smr.transform.localScale.x /
					                                                       skeleton.transform.localScale.x);
					ci.transform = MatrixAddPos(ci.transform, smr.transform);
				}
				else if (type == 1)

					ci.transform = GetScalePosOriginMatrix(smr);


				else if (type == -1)
				{
					ci.transform = Matrix4x4.identity;
				}
				else if (type == 2)
				{
					ci.transform = Matrix4x4.identity;
					ci.transform = MatrixAddPos(ci.transform, smr.transform);
				}
				else if (type == 3)
				{
					//第一个基地模型，不需要缩放；其他模型，还是使用附加缩放 
					//！！有风险啊！！，需要注意！！
					//！！也不好维护！！！
					if (i == 0)
					{
						ci.transform = Matrix4x4.identity;
					}
					else
					{
						ci.transform = GetScaleGuMatrix(smr);
						ci.transform = MatrixAddPos(ci.transform, smr.transform);
					}
				}

				ci.subMeshIndex = sub;
				combineInstances.Add(ci);
			}


			#endregion

			// Collect bones
			for (int j = 0; j < smr.bones.Length; j++)
			{
				int tBase = 0;
				for (tBase = 0; tBase < transforms.Count; tBase++)
				{
					if (smr.bones[j].name.Equals(transforms[tBase].name))
					{
						bones.Add(transforms[tBase]);
						break;
					}
				}
			}
			
		}

        // merge materials
		if (combine)
		{
			newMaterial = new Material (Shader.Find ("Mobile/Diffuse"));
			oldUV = new List<Vector2[]>();
			// merge the texture
			List<Texture2D> Textures = new List<Texture2D>();
			for (int i = 0; i < materials.Count; i++)
			{
				Textures.Add(materials[i].GetTexture(COMBINE_DIFFUSE_TEXTURE) as Texture2D);
			}

			newDiffuseTex = new Texture2D(COMBINE_TEXTURE_MAX, COMBINE_TEXTURE_MAX, TextureFormat.RGBA32, true);
			Rect[] uvs = newDiffuseTex.PackTextures(Textures.ToArray(), 0);
			newMaterial.mainTexture = newDiffuseTex;

            // reset uv
			Vector2[] uva, uvb;
			for (int j = 0; j < combineInstances.Count; j++)
			{
				uva = (Vector2[])(combineInstances[j].mesh.uv);
				uvb = new Vector2[uva.Length];
				for (int k = 0; k < uva.Length; k++)
				{
					uvb[k] = new Vector2((uva[k].x * uvs[j].width) + uvs[j].x, (uva[k].y * uvs[j].height) + uvs[j].y);
				}
				oldUV.Add(combineInstances[j].mesh.uv);
				combineInstances[j].mesh.uv = uvb;
			}
		}

		// Create a new SkinnedMeshRenderer
		//SkinnedMeshRenderer oldSKinned = skeleton.GetComponent<SkinnedMeshRenderer> ();
		SkinnedMeshRenderer oldSKinned = skeleton.GetComponentInChildren<SkinnedMeshRenderer>();
		Transform skeletonTrans = oldSKinned.transform;
		if (oldSKinned != null) {

			GameObject.DestroyImmediate (oldSKinned);
		}
		SkinnedMeshRenderer r = skeletonTrans.gameObject.AddComponent<SkinnedMeshRenderer>();
		r.sharedMesh = new Mesh();
		//r.sharedMesh.CombineMeshes(combineInstances.ToArray(), combine, false);// Combine meshes
		r.sharedMesh.CombineMeshes(combineInstances.ToArray(), combine, true);// Combine meshes
		r.bones = bones.ToArray();// Use new bones
		r.sharedMesh.RecalculateBounds();
		r.sharedMesh.RecalculateNormals();
		r.sharedMesh.RecalculateTangents();
		if (combine)
		{
			r.material = newMaterial;
			for (int i = 0 ; i < combineInstances.Count ; i ++)
			{
				combineInstances[i].mesh.uv = oldUV[i];
			}
		}else
		{
			r.materials = materials.ToArray();
		}
	}
}
