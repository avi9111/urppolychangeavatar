/* v1.1 修复 Save ShareMesh 的 uv,tangent,normal 等
 *
 * 
 */
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
/// <summary>
/// 追加 skinnedMeshRender 的 shaderMesh:过程不可逆，需注意使用
/// </summary>
public class CharacterSkinned4Add : MonoBehaviour
{
    public enum BindPosesTransform
    {
        None,
        ExceptSelf,
        World,
        加平移矩阵
    }

    public enum Axle
    {
        X,
        Y,
        Z
    }
    public bool IsPreview = true;
    public bool IsScaleRevert;
    [Header("缩小 N 倍后复制")]
    public float copyMeshScale=0f;

    public Mesh resetMesh;


    public BindPosesTransform _meshTransformType;
    public Transform _meshTransformLocate;
    void Start()
    {
        
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="Position"></param>
    /// <param name="angle">记得，需要 x Mathf.Deg2Rad 后的弧度值</param>
    /// <returns></returns>
    Vector3 DoTwistX(Vector3 Position, float angle)
    {
        Vector3 result = Vector3.zero;
        float CosAngle = Mathf.Cos(angle);
        float SinAngle = Mathf.Sin(angle);
        result.x = Position.x;
        result.y = Position.y * CosAngle + SinAngle * Position.z;      
        result.z = -Position.y * SinAngle + CosAngle * Position.z;
        return result;
    }
    //围绕着Y轴旋转
    Vector3 DoTwistY(Vector3 Position,float angle)
    {
        Vector3 result = Vector3.zero;
        float CosAngle = Mathf.Cos(angle);
        float SinAngle = Mathf.Sin(angle);
        result.x = Position.x * CosAngle - SinAngle * Position.z;
        result.y = Position.y;
        result.z = Position.x * SinAngle + CosAngle * Position.z;
        return result;
    }
    //围绕Z轴旋转
    Vector3 DoTwistZ(Vector3 Position, float angle)
    {
        Vector3 result = Vector3.zero;
        float CosAngle = Mathf.Cos(angle);
        float SinAngle = Mathf.Sin(angle);
        result.x = Position.x * CosAngle + SinAngle * Position.y;
        result.y = -Position.x * SinAngle + CosAngle * Position.y;
        result.z = Position.z;
        return result;
    }

    public void BeforeCloseAndResetMesh(SkinnedMeshRenderer smrOrigin, Mesh mesh,int bindIndex)
    {
        //var meshOrigin = smr.sharedMesh;
        
        //mesh.bindposes = meshOrigin.bindposes;
        //mesh.bounds = meshOrigin.bounds;
        //mesh.vertices 
        SetSkinnedBoneWeight(smrOrigin.transform,ref mesh,smrOrigin.bones, bindIndex,BindPosesTransform.ExceptSelf);
        smrOrigin.sharedMesh = mesh;

    }
    /// <summary>
    /// 绑定 Mesh 的骨骼（需要原 SkinnedMeshRenderer 的 mesh 和 绑定的 mesh "相近"
    /// </summary>
    /// <param name="addedGameObject"></param>
    /// <param name="selected"></param>
    public void BeforeCloseSkinnedBindderWindow(GameObject addedGameObject, Transform selected)
    {
        //原Avatar 和 Added Avatar需要是同源，所以用可用用当前 gameObject 计算index，但设置要是目标 smr
        var smr = gameObject.GetComponentInChildren<SkinnedMeshRenderer>();
        var lst = new List<Transform>();
        lst.AddRange(smr.bones);
        var bindIndex = lst.IndexOf(selected);
        //模式1，直接加载 Mesh
        if (resetMesh!=null)
        {
            Debug.Log("采用模式1，Add Mesh Bones");
            BeforeCloseAndResetMesh(smr, resetMesh, bindIndex);
            return;
        }

        
        //模式2 ，加载 GameObject
        Debug.Log("采用模式2，Add Mesh Bones");
        var smrAdded = addedGameObject.GetComponentInChildren<SkinnedMeshRenderer>();
        //若骨骼+mesh有缩放，则需要反向缩放
        Mesh mesh = null;
        if (smrAdded.transform.localScale != Vector3.one)
        {
            Vector3[] vertices = new Vector3[smrAdded.sharedMesh.vertexCount];
            var matrix = Matrix4x4.identity;
            //先平移后缩放
            if (_meshTransformType == BindPosesTransform.加平移矩阵)
            {
                Debug.LogError("before " + matrix);
                matrix *= GetPosMatrix(smrAdded,_meshTransformLocate);
                Debug.LogError("end " + matrix);
            }
            matrix *=GetScaleNoPosMatrix(smrAdded); //TODO:对于谷爱你的模型，子缩放==0.01 好像不起作用

            // var scale2 = GetScaleValMatrix(2);
            // //var rot = smrAdded.transform.localEulerAngles;
            // //SkinnedMeshRenderer 在子节点，父节点是 Aimator 所以
            var rot = smrAdded.transform.eulerAngles;
        
            //Debug.LogError("旋转矩阵测试 rotX=" + rot.x);
            for (int i = 0; i < smrAdded.sharedMesh.vertexCount; i++)
            {
                var vert = smrAdded.sharedMesh.vertices[i];
                vertices[i] = vert;
                if (rot.x != 0)//这个旋转矩阵，有点问题哦
                {
                    vertices[i] = DoTwistX(vertices[i], (-rot.x) * Mathf.Deg2Rad);
                }
                if(rot.y !=0){
                    vertices[i] = DoTwistY(vertices[i], (-rot.y) * Mathf.Deg2Rad);
                }

                if (rot.z != 0)
                {
                    vertices[i] = DoTwistZ(vertices[i],(-rot.z)* Mathf.Deg2Rad);
                }

                //可能不需要缩放，当前帧的搞不清楚
                if(IsScaleRevert)
                    vertices[i] = matrix *  vertices[i];
                
            }
            

            if (IsPreview)
            {
                var share = smr.sharedMesh;
                //TODO:这个初始方法，好像有些问题，待排查
                mesh = new Mesh();
                mesh.vertices = vertices;
                mesh.bounds = smrAdded.sharedMesh.bounds;//这个应该是有一个中心点（bounds center?还是什么不确定)，控制位置的
                mesh.bindposes = smrAdded.sharedMesh.bindposes;
                mesh.boneWeights = smrAdded.sharedMesh.boneWeights;
                mesh.triangles = smrAdded.sharedMesh.triangles;
                mesh.uv = share.uv;
                mesh.uv2 = share.uv2;
                mesh.uv3 = share.uv3;
                mesh.uv4 = share.uv4;
                mesh.uv5 = share.uv5;
                mesh.uv6 = share.uv6;
                mesh.uv7 = share.uv7;
                mesh.uv8 = share.uv8;
                //mesh.RecalculateBounds();
                //mesh.RecalculateNormals();
                //mesh.RecalculateTangents();
                if (smrAdded.name.Contains("_added"))
                {
                    mesh.name = smrAdded.name;
                }
                else
                {
                    mesh.name = smrAdded.name + "_added";
                }
                
                smrAdded.sharedMesh = mesh;
                
            }
            else
            {

                smrAdded.sharedMesh.vertices = vertices;
                mesh = smrAdded.sharedMesh;
            }

        }

        
        SetSkinnedBoneWeight(smrAdded.transform,ref mesh,smrAdded.bones,bindIndex);
        smrAdded.sharedMesh = mesh;
    }
    
    /// <summary>
    /// TODO:网上找的 方法，好像有些问题，会变扁平。。。。。
    /// </summary>
    /// <param name="axle"></param>
    /// <param name="angle"></param>
    /// <returns></returns>
    Matrix4x4 GetRotationMatrix(Axle axle, float angle)
    {
        var matrix = Matrix4x4.identity;

        // set matrix
        if (axle == Axle.X)
        {
            matrix.m11 = Mathf.Cos(angle * Mathf.Deg2Rad);
            matrix.m22 = -Mathf.Sin(angle * Mathf.Deg2Rad);
            matrix.m21 = Mathf.Sin(angle * Mathf.Deg2Rad);
            matrix.m22 = Mathf.Cos(angle * Mathf.Deg2Rad);
        }
        else if (axle == Axle.Y)
        {
            matrix.m00 = Mathf.Cos(angle * Mathf.Deg2Rad);
            matrix.m02 = Mathf.Sin(angle * Mathf.Deg2Rad);
            matrix.m20 = -Mathf.Sin(angle * Mathf.Deg2Rad);
            matrix.m22 = Mathf.Cos(angle * Mathf.Deg2Rad);
        }
        else if (axle == Axle.Z)
        {
            matrix.m00 = Mathf.Cos(angle * Mathf.Deg2Rad);
            matrix.m01 = -Mathf.Sin(angle * Mathf.Deg2Rad);
            matrix.m10 = Mathf.Sin(angle * Mathf.Deg2Rad);
            matrix.m11 = Mathf.Cos(angle * Mathf.Deg2Rad);
        }

        return matrix;
    }

    public static Matrix4x4 GetScaleValMatrix(float v)
    {
        Matrix4x4 matrix = Matrix4x4.identity;
        matrix.m00 = v;
        matrix.m11 = v;
        matrix.m22 = v;    
        return matrix;
    }

    Matrix4x4 GetPosMatrix(SkinnedMeshRenderer smr,Transform trans)
    {
        Matrix4x4 matrix = Matrix4x4.identity;
        var position = trans.position;
        matrix.m03 = -position.x;
        matrix.m13 = -position.y;
        matrix.m23 = -position.z;
        return matrix;
    }

    Matrix4x4 GetScaleNoPosMatrix(SkinnedMeshRenderer smr)
    {
        Matrix4x4 matrix = Matrix4x4.identity;
        var trans = smr.transform;
        var scale = trans.lossyScale;
        Debug.LogError("专业化矩阵测试scale=" + scale);

        matrix.m00 = scale.x;
        matrix.m11 = scale.y;
        matrix.m22 = scale.z;
		
        // var position = trans.position;
        // matrix.m03 = -position.x;
        // matrix.m13 = -position.y;
        // matrix.m23 = -position.z;
        return matrix;
    }
    Matrix4x4 GetScaleOnlyMatrix(SkinnedMeshRenderer smr)
    {
        Matrix4x4 matrix = Matrix4x4.identity;
        var trans = smr.transform;
        var scale = trans.lossyScale;
		
        matrix.m00 = scale.x;
        matrix.m11 = scale.y;
        matrix.m22 = scale.z;
        
        return matrix;
    }
    

    /// <summary>
    /// 以当前 SkinnedMeshReneder 的 Avator 做基础，弹出，；
    /// 而新的 go (含SkinnedMeshRenderer）可能未绑定，需要重新绑定
    /// </summary>
    /// <param name="go"></param>
    public void PreAddSkinnedMesh(GameObject go)
    {
        var smr = gameObject.GetComponentInChildren<SkinnedMeshRenderer>();
        if (smr == null)
        {
            Debug.LogWarning("缺 SkinnedMeshRenderer");
            return;
        }

        //smr.bones;

    }

    public void Add2SkinnedMesh(Mesh mesh)
    {
        var smr = gameObject.GetComponentInChildren<SkinnedMeshRenderer>();
        if (smr == null)
        {
            Debug.LogWarning("缺 SkinnedMeshRenderer");
            return;
        }
        //TODO:1.当前组件是一个skinnedMeshRender，替换了mesh，需要右键，然后调用这个方法重新绑定这mesh；
        //SetSkinnedBoneWeight(smr);
        //TODO:2.直接add mesh的方法之后再弄吧
        
        //现在只是先手动，改mesh,
        
        
    }
    /// <summary>
    /// 当前组件是一个skinnedMeshRender，替换了mesh，需要右键，然后调用这个方法重新绑定这mesh；
    /// 功能只是绑 pos for mesh,数组有些绕，主要为了演示代码用
    /// </summary>
    /// <param name="smr">待设置的对象</param>
    public static void SetSkinnedBoneWeight(Transform transform, ref Mesh mesh, Transform[] bones,
        int bindBoneIndex = 0,
        BindPosesTransform bindPosesTrans = BindPosesTransform.World)
    {
        //var transform = smr.transform;
        //List<Transform> bones = new List<Transform>();
        //bones.AddRange(smr.bones);
        
        List<Matrix4x4> bindposes = new List<Matrix4x4>();

        for( int b = 0; b < bones.Length; b++ ) {
            if (bindPosesTrans == BindPosesTransform.None)
            {
                bindposes.Add(bones[b].worldToLocalMatrix);
            }
            else if (bindPosesTrans == BindPosesTransform.ExceptSelf)
            {
                bindposes.Add(bones[b].worldToLocalMatrix *
                              GetScaleValMatrix(transform.lossyScale.x / transform.localScale.x));
            }
            else
            {
                //大错特错的一个公式
                //哪个天才写的公式？？
                bindposes.Add(bones[b].worldToLocalMatrix * transform.worldToLocalMatrix);
            }
        }
        //var mesh = smr.sharedMesh;
        mesh.bindposes = bindposes.ToArray();
        var weights = new BoneWeight[mesh.vertexCount];
        for (int i = 0; i < weights.Length; i++)
        {
            weights[i].boneIndex0 = bindBoneIndex;
            weights[i].weight0 = 1;
        }

        mesh.boneWeights = weights;
    }
}
