【2018年前：】
原来是网上大神的分享项目，腾讯开发者论坛也有这个分享
是一个比较稳定（Editor版本旧），有效果（扩展麻烦），UI（传统最简单GUI，不受UGUI还是NGUI限制)，
而且代码和备注都比较清晰不错的版本

【v1.2-2021.12.25-by avi9111：】
现基于2021，项目刚好用到URP，所以将这版本改为URP版本（半修复)，
注意：非URP不能直接用

【使用方法：】
打开场景：LowPolyMainScene.unity
运行
右边，有2个重复按钮，最下，
点击即可看到换了5个部件的效果，其中4个原部件+1重复新增部件
之后需要自己二次开发代码

【限制：】
和原来限制一样，所有部件必须同一骨骼;
没有提供pro 扩展修复的代码;
这只是一个普通能用的URP换装Demo;

【目录---随心记】			-整个了一些第三方项目
/UnityBoneVisualizer 		- bone的可视化(貌似是直接读的transform，并不经过skinnedMeshRenderer
/ObjectsInjector			-没用，只是骨骼节点下硬add GameObject项目，带一些旋转角度代码
/Editor				-右键，组合mesh等功能		