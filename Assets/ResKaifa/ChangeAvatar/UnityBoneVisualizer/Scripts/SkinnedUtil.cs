using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkinnedUtil 
{
    
    public static void ReadAllBones(GameObject go)
    {
        var smr = go.GetComponentInChildren<SkinnedMeshRenderer>();
        if (smr == null) return;

        foreach (var bone in smr.bones)
        {
            Debug.LogError(bone.name);
        }
    }
}
