using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CharacterFashionData : SpecSingleton<CharacterFashionData>
{
    public struct FashionItem
    {
        //TODO:还是改Enum吧，虽然 demo 可随意
        /// <summary>
        /// 1-attack;2-defend;3-balcksmith;4-ring；
        /// (0-未分类）
        /// -1 - 全部
        /// </summary>
        public int type;
        public string icon;
        public  string prefab;
    }

    private List<FashionItem> _lst = new List<FashionItem>();
    public override void Dispose()
    {
        _lst.Clear();
    }
    
    public void Init()
    {
        _lst.Clear();
        var i1 = new FashionItem {type=1,icon = "boardPurple", prefab = ""};
        var i2 = new FashionItem {type=1,icon = "boardGuo", prefab = ""};
        var i3 = new FashionItem {type=2,icon = "hatSnowWhite", prefab = "Character_Snow_Female_01_Snow"};
        var i4 = new FashionItem {type=2,icon = "hatMotor", prefab = "Character_Snow_Female_Motor"};
        var i5 = new FashionItem {type=2,icon = "hatRare", prefab = "Character_Snow_Female_Rare"};
        var i6 = new FashionItem {type=2,icon = "hatNorth", prefab = "Character_Snow_Female_default"};
        
        _lst.Add(i1);
        _lst.Add(i2);
        _lst.Add(i3);
        _lst.Add(i4);
        _lst.Add(i5);
        _lst.Add(i6);
    }

    public List<FashionItem> Get(int type)
    {
        if (type == -1)
        {
            return _lst;
        }
        
        return _lst.Where(i => i.type == type).ToList();
        
    }
}
