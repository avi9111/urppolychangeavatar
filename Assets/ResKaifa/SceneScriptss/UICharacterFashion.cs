using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UICharacterFashion : UISnowGuBase
{
    private Transform _focusTrans;
    private Transform _bagItemContent;
    private GameObject _bagItem;
    private GameObject _bagEmptyItem;
    private GameObject _smr;
    
    private GameObject _smrAdded;

    private GameObject _skillRun;
    private GameObject _skillIdle;
    private GameObject _btnEnterGame;
    public RuntimeAnimatorController _animCtl;
    protected override void Awake()
    {
        base.Awake();
        CharacterFashionData.Inst.Init();  
        

    }

    protected override void Start()
    {
         
        EventTriggerListener.Get(base.GetObj("btnAttack")).onClick += OnAttackClick;
        _focusTrans = GetTrans("FocusLine");
        EventTriggerListener.Get(GetObj("btnAll")).onClick += OnAllClick;
        EventTriggerListener.Get(GetObj("btnDefend")).onClick += OnHatClick;
        EventTriggerListener.Get(GetObj("btnBlackSmith")).onClick += OnBlackSmithClick;
        EventTriggerListener.Get(GetObj("btnRing")).onClick += OnRingClick;

        _skillIdle = GetObj("SkillIcon_Run");
        _skillRun = GetObj("SkillIcon_Idle");
        _btnEnterGame = GetObj("Button_AutoSelect");
        _animCtl = GetAnimator("GuAliController");
        EventTriggerListener.Get(_skillIdle).onClick += OnSkillIdleClick;
        EventTriggerListener.Get(_skillRun).onClick += OnSkillRunClick;
        EventTriggerListener.Get(_btnEnterGame).onClick += OnEnterGameClick;
        _bagItem = GetObj("Slot_Yellow");
        _bagEmptyItem = GetObj("Slot_Empty");
        _bagItemContent = GetObj("Slot_Content").transform;
        
        RefreshIconByList(CharacterFashionData.Inst.Get(-1));

        _smr = GetObj("Character_Skier_01");
        _smrAdded = GetObj("Character_Skier_01_1");
        //开始即刻 换装
        //zh注意！，必须保证至少有6个基本数据
        //DressHead(CharacterFashionData.Inst.Get(-1)[5]);
    }

    void OnSkillIdleClick(GameObject sender)
    {
        _smr.GetComponent<Animator>().Play("Idle");
    }

    void OnSkillRunClick(GameObject sender)
    {
        _smr.GetComponent<Animator>().Play("RunForward");
    }

    void OnEnterGameClick(GameObject sender)
    {
    }

    /// <summary>
    /// 戒指分组
    /// </summary>
    /// <param name="sender"></param>
    void OnRingClick(GameObject sender)
    {
        _focusTrans.SetParent(sender.transform, false);
    }

    void OnBlackSmithClick(GameObject sender)
    {
        _focusTrans.SetParent(sender.transform, false);
    }

    void OnHatClick(GameObject sender)
    {
        _focusTrans.SetParent(sender.transform, false);
    }

    void OnAllClick(GameObject sender)
    {
        _focusTrans.SetParent(sender.transform, false);
    }

    void OnAttackClick(GameObject sender)
    {
        
        _focusTrans.SetParent(sender.transform, false);
    }


    void RefreshIconByList(List<CharacterFashionData.FashionItem> items)
    {
        for (int i = _bagItemContent.childCount - 1; i >= 0; i--)
        {
            if (_bagItemContent.GetChild(i).gameObject == _bagItem
            || _bagItemContent.GetChild(i).gameObject==_bagEmptyItem)
            {
                continue;
            }

            GameObject.Destroy(_bagItemContent.GetChild(i).gameObject);
        }

        foreach (var item in items)
        {
            var newItem = GameObject.Instantiate(_bagItem, _bagItemContent.transform, false);
            newItem.SetActive(true);
            
            var sprite = newItem.transform.Find("Slot_Item/Item").GetComponent<Image>();
            //EventTriggerListener.Get(sprite.gameObject).onClick += OnBagItemClick;
            EventTriggerListener.Get(newItem).onClick += OnBagItemClick;
            //-------------- 应该是挺耗性能的，暂时用用 ---------
            //var container = sprite.gameObject.AddComponent<UIDataContainer>();
            var container = newItem.AddComponent<UIDataContainer>();
            container.AData = item;
            //-------------------------------
            var t2d = ResMgr.Inst.LoadResource(item.icon);
            //一些 texture2d处理，参考
            //https://blog.csdn.net/iningwei/article/details/88537706
            sprite.sprite = Sprite.Create(t2d, new Rect(0, 0, t2d.width, t2d.height), Vector2.zero);
            
        }

        //补上一些空格，免得太空
        for (int i = 0; i < 36 - 4; i++)
        {
            var newEmpty = Instantiate(_bagEmptyItem, _bagItemContent.transform, false);
            newEmpty.SetActive(true);
        }
    }

    void OnBagItemClick(GameObject sender)
    {
        var container = sender.GetComponent<UIDataContainer>();
        var item = container.AData is CharacterFashionData.FashionItem ? (CharacterFashionData.FashionItem) container.AData : default;
        Debug.LogError("click icon=" + item.icon);

        if (item.type == 2)
        {
            DressHead(item);
        }

    }

    void DressHead(CharacterFashionData.FashionItem item)
    {
        _smr.GetComponent<Animator>().enabled = false;
        _smr.GetComponent<Animator>().runtimeAnimatorController = null;
        var smr = _smr.GetComponentInChildren<SkinnedMeshRenderer>();
        smr.sharedMesh = ResMgr.Inst.LoadMesh("Character_Snow_Female_01_copy");
        var skinnedSMR = _smrAdded.GetComponentInChildren<SkinnedMeshRenderer>();
        skinnedSMR.sharedMesh = ResMgr.Inst.LoadMesh(item.prefab);
        List<GameObject> bodies = new List<GameObject>();
        bodies.Add(_smr);
        bodies.Add(_smrAdded);
        UCharacterController.CombinBodies(bodies, 3,false,false);

        StartCoroutine(DelayAnim());
    }

    IEnumerator DelayAnim()
    {
        //yield return new WaitForSeconds(0.001f);
        _smr.GetComponent<Animator>().runtimeAnimatorController = _animCtl;
        _smr.GetComponent<Animator>().enabled = true;

        yield return null;
    }
}
