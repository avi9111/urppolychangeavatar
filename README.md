# URPPolyChangeAvatar
一个换装项目，基于URP，迭代和集成不多，仅仅做Demo，之后会加入捏脸（和平精英模板），但应该也不会更新了

![UI效果图](Images/uiCharacter.png)
## Roadmap，更新路线图

之前花了2个星期做的，换头 Demo 挺简单的鸭，一直没做市场验证；最近再用，发现代码里都是天坑，又花了2天作”紧急修复"；结果你懂的,修复本身又引入不少坑；所以修复后就还是决定先不更了。

应该不太会更新，看看这一堆糟心事(待完成)：
- 支持多个模型换装	（网上各种DMM 模资源）
- 支持表情捏脸
- 进入场景		（动画 或U Ramp)
- 删除，还原成默认着装（一个按钮功能而已）
- 滑板换装支持	（武器）
- 其他配饰支持	（背景，涂鸦等）
- CloseUp		（近景大头切换)
- ~~清掉多余ICON	（暂时隐藏）~~
- 动作，动作
- ~~换装时间太长，要优化一下。。。~~
- Resources转AddressableAsset
- 支持非URP
- Text方案
- 动作fbx文件压缩
- gitlab自动构建

![捏脸工作中](Images/nielian.jpg)


## Getting started
此项目依赖URP，部分UI，所以需要以下2个步骤才能正常打开：

- [ ] Windows->PackageManager,安装URP
  
  PackageManager界面，搜索：univ 即可
![安装URP截图](Images/needURP.png)
  
- [ ] 安装Universal RP 后，配置 Pipline
  
  Edit->Project Settings...
  ![配置 URP Pipline截图](Images/needPipline.png)
  
- [ ] 打开场景 MyRampCharacter 时，自动弹出提示，安装TextMeshPro



## Test and Deploy

自测过一次，2019无法正常打开，请使用2020及以上版本

初始场景：ResKaifa/Scenes/MyRampCharacter.unity

## Usage
打开场景即可使用换装功能，结构大概是:

Scene->Canvas->Elements->UIController->CombinAvatar


## Project status
暂停开发，各种演示中

